# README #

This README is intended to explain how to build and run the web scraper

### How do I get set up? ###

* Make sure to have Go installed on your machine
* Use a terminal to Navigate to the root folder of this project
* type "go build" and press enter to build an executable file
* Run the executable file
* Alternatively, type "go run GWPrices.go" and press enter to run the program in your terminal
